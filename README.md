# a-frame-workspace

A-Frame workspace

## About

[A-Frame](https://aframe.io/) is a web framework for building virtual reality experiences by [Supermedium](https://www.supermedium.com/).

## Use this repo

Testing a monorepo approach for this one.
Run
```
yarn
```
in root to install.
Run
```
yarn dev
```
inside of a package to run it.
