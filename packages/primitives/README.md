# Primitives

A-Frame primitive geometry without use of imported models.
Uses two simple custom components, **rotator** and **color-jump**.
