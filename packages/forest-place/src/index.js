/**
 * A-Frame imports
 */
// eslint-disable-next-line no-unused-vars
import * as AFRAME from 'aframe'
// eslint-disable-next-line no-unused-vars
import * as ENV from 'aframe-environment-component'
// eslint-disable-next-line no-unused-vars
import * as HANDS from 'super-hands'

import { init as initComponents } from './components'

init()
function init () {
  initComponents()
}
